package com.space.recipes.common_data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Recipe(
    val chef: String?,
    val photo: String?,
    val description: String?,
    val calories: Double?,
    val title: String?,
    val tags: List<String>?,
) :
    Parcelable {
    companion object {
        val empty = Recipe(
            chef = null,
            photo = null,
            description = null,
            calories = null,
            title = null,
            tags = null,
        )
    }
}
