package com.space.recipes.common_data

data class ErrorResponse(
    val errorCode: String
)
