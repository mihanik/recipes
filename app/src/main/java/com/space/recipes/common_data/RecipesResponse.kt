package com.space.recipes.common_data

sealed class RecipesResponse<T, ET> {
    data class Success<T, ET>(val data: T?) : RecipesResponse<T, ET>()
    data class Error<T, ET>(val error: ET?) : RecipesResponse<T, ET>()

    fun <E> convert(converter: (T?) -> E): RecipesResponse<E, ET> = when (this) {
        is Success -> Success(converter(data))
        is Error -> Error(error)
    }
}
