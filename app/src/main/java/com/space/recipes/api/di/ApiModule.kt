package com.space.recipes.api.di

import com.space.recipes.api.NetApiClient
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module
class ApiModule {
    @Provides
    @Reusable
    fun provideNetApiClient(): NetApiClient = NetApiClient()
}
