package com.space.recipes.api

import com.contentful.java.cda.CDAAsset
import com.contentful.java.cda.CDAClient
import com.contentful.java.cda.CDAEntry
import com.contentful.java.cda.image.ImageOption
import com.space.recipes.common_data.Recipe

class NetApiClient() {
    private var clientDelivery: CDAClient = CDAClient.builder()
        .setToken(CONTENTFUL_DELIVERY_TOKEN)
        .setSpace(CONTENTFUL_SPACE_ID)
        .build()

    suspend fun fetchRecipe(): List<Recipe> {
        return try {
            clientDelivery
                .fetch(CDAEntry::class.java)
                .withContentType("recipe")
                .all()
                .items()
                .map {
                    fromEntry(
                        it as CDAEntry,
                        DEFAULT_LOCALE
                    )
                }
        } catch (throwable: Throwable) {
            throw Exception(throwable)
        }
    }

    fun fromEntry(entry: CDAEntry, locale: String): Recipe = Recipe(
        entry.getField<CDAEntry?>(DEFAULT_LOCALE, "chef")
            ?.getField<String?>(DEFAULT_LOCALE, "name"),
        try {
            entry.getField<CDAAsset?>(locale, "photo")
                ?.urlForImageWith(
                    ImageOption.https(),
                    ImageOption.formatOf(ImageOption.Format.webp)
                )
                .orEmpty()
        } catch (_: Throwable) {
            ""
        },
        entry.getField<String?>(locale, "description").orEmpty(),
        entry.getField<Double?>(locale, "calories"),
        entry.getField<String?>(locale, "title").orEmpty(),
        entry.getField<List<CDAEntry>?>(locale, "tags")?.map {
            it.getField<String?>("name").orEmpty()
        },
    )

    companion object {
        private const val DEFAULT_LOCALE = "en-US"
        private const val CONTENTFUL_DELIVERY_TOKEN =
            "7ac531648a1b5e1dab6c18b0979f822a5aad0fe5f1109829b8a197eb2be4b84c"
        private const val CONTENTFUL_SPACE_ID = "kk2bw5ojx476"
    }
}
