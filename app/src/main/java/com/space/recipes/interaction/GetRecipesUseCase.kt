package com.space.recipes.interaction

import com.space.data.repository.RecipesDataRepository
import com.space.recipes.common_data.Recipe
import com.space.recipes.common_data.RecipesResponse
import javax.inject.Inject

interface GetRecipesUseCase {
    suspend operator fun invoke(): RecipesResponse<List<Recipe>, Exception?>
}

class GetRecipesUseCaseImpl @Inject constructor(private val dataRepository: RecipesDataRepository) :
    GetRecipesUseCase {
    override suspend fun invoke() = dataRepository.getData()
}
