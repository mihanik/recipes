package com.space.recipes.interaction.di

import com.space.recipes.interaction.GetRecipesUseCase
import com.space.recipes.interaction.GetRecipesUseCaseImpl
import dagger.Binds
import dagger.Module

@Module
abstract class UseCaseModule {
    @Binds
    abstract fun bindGetRecipesUseCase(useCase: GetRecipesUseCaseImpl): GetRecipesUseCase
}
