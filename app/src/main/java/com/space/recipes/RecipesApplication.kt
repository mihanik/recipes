package com.space.recipes

import android.app.Application
import com.space.recipes.di.ApplicationComponent
import com.space.recipes.di.DaggerApplicationComponent

class RecipesApplication : Application() {
    @Suppress("MemberVisibilityCanBePrivate")
    val appComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent.factory()
            .create(
                application = this,
            )
    }

    override fun onCreate() {
        super.onCreate()
        appComponent.inject(this)
    }
}
