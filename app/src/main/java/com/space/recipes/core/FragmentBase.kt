package com.space.recipes.core

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.space.recipes.RecipesApplication
import com.space.recipes.di.ApplicationComponent
import javax.inject.Inject

open class FragmentBase : Fragment() {
    val appComponent: ApplicationComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        (activity?.application as RecipesApplication).appComponent
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
}
