package com.space.recipes.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.space.recipes.databinding.MainActivityBinding

class MainActivity : AppCompatActivity() {
    private val bindings by lazy { MainActivityBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(bindings.root)
    }
}
