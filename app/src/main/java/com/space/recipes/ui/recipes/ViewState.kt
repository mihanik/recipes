package com.space.recipes.ui.recipes

import com.space.recipes.common_data.Recipe

sealed class ViewState {
    data class SuccessState(val result: List<Recipe>?) : ViewState()
    class ErrorState : ViewState()
    class InitialState : ViewState()
}
