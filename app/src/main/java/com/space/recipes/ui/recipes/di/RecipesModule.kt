package com.space.recipes.ui.recipes.di

import androidx.lifecycle.ViewModel
import com.space.recipes.di.ViewModelKey
import com.space.recipes.ui.recipes.RecipesViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class RecipesModule {
    @Binds
    @IntoMap
    @ViewModelKey(RecipesViewModel::class)
    internal abstract fun bindViewModel(viewModel: RecipesViewModel): ViewModel
}
