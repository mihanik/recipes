package com.space.recipes.ui.recipes

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.space.recipes.common_data.RecipesResponse
import com.space.recipes.interaction.GetRecipesUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class RecipesViewModel @Inject constructor(
    private val getRecipesUseCase: GetRecipesUseCase
) : ViewModel() {

    fun loadRecipes() {
        viewModelScope.launch(Dispatchers.IO) {
            _recipesState.value = when (val response = getRecipesUseCase()) {
                is RecipesResponse.Success -> ViewState.SuccessState(response.data)
                is RecipesResponse.Error -> ViewState.ErrorState()
            }
        }
    }

    private val _recipesState: MutableStateFlow<ViewState> =
        MutableStateFlow(ViewState.InitialState())
    val recipesState: LiveData<ViewState>
        get() = _recipesState.asLiveData()
}
