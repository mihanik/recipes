package com.space.recipes.ui.recipes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.space.recipes.adapter.RecipesAdapter
import com.space.recipes.core.FragmentBase
import com.space.recipes.databinding.RecipesFragmentBinding

class RecipesFragment : FragmentBase() {

    private lateinit var binding: RecipesFragmentBinding

    val viewModel: RecipesViewModel by viewModels { viewModelFactory }
    private val recipesAdapter by lazy { RecipesAdapter() }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        appComponent.inject(this)
        binding = RecipesFragmentBinding.inflate(layoutInflater)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding) {
            with(recyclerView) {
                layoutManager = LinearLayoutManager(requireContext())
                adapter = recipesAdapter
            }
        }

        recipesAdapter.setListener {
            findNavController().navigate(RecipesFragmentDirections.recipesToDetails(it))
        }

        viewModel.recipesState.observe(viewLifecycleOwner) {
            when (it) {
                is ViewState.InitialState -> {
                    binding.progressLoading.isVisible = true
                }
                is ViewState.ErrorState -> {
                    binding.progressLoading.isVisible = false
                }
                is ViewState.SuccessState -> {
                    binding.progressLoading.isVisible = false
                    recipesAdapter.submitList(it.result)
                }
            }
        }
        viewModel.loadRecipes()
    }
}
