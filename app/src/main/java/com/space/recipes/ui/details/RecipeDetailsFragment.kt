package com.space.recipes.ui.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.space.recipes.R
import com.space.recipes.common_data.Recipe
import com.space.recipes.core.FragmentBase
import com.space.recipes.databinding.RecipeDetailsFragmentBinding
import com.space.recipes.extantions.loadImage

class RecipeDetailsFragment : FragmentBase() {
    private lateinit var binding: RecipeDetailsFragmentBinding

    private val args: RecipeDetailsFragmentArgs by navArgs()
    private val recipe: Recipe by lazy { args.recipe }

    private lateinit var viewModel: RecipeDatailsViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        appComponent.inject(this)
        binding = RecipeDetailsFragmentBinding.inflate(layoutInflater)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding) {
            titleTextView.text = recipe.title ?: getString(R.string.recipe_default_title)
            recipe.photo?.let { recipeImageView.loadImage(it) }
            descriptionTextView.text = recipe.description.orEmpty()
            chefNameTextView.text = recipe.chef ?: getString(R.string.recipe_default_chef_name)
            tegsTextView.text = recipe.tags?.joinToString(separator = ", ")
        }
    }
}
