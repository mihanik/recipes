package com.space.recipes.di

import android.content.Context
import com.space.recipes.ui.MainActivity
import dagger.Binds
import dagger.Module

@Module
abstract class ApplicationModule {
    abstract fun contributeMainAndroidInjector(): MainActivity

    @Binds
    abstract fun bindContext(mainActivity: MainActivity): Context
}
