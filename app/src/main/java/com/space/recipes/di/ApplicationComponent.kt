package com.space.recipes.di

import android.app.Application
import android.content.Context
import com.space.data.repository.di.RepositoryModule
import com.space.recipes.RecipesApplication
import com.space.recipes.api.di.ApiModule
import com.space.recipes.interaction.di.UseCaseModule
import com.space.recipes.ui.details.RecipeDetailsFragment
import com.space.recipes.ui.recipes.RecipesFragment
import com.space.recipes.ui.recipes.di.RecipesModule
import dagger.Binds
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppContextModule::class,
        ViewModelModule::class,
        ApplicationModule::class,
        UseCaseModule::class,
        RecipesModule::class,
        RepositoryModule::class,
        ApiModule::class,
    ]
)
interface ApplicationComponent {
    @Component.Factory
    interface Builder {
        fun create(
            @BindsInstance
            application: Application,
        ): ApplicationComponent
    }

    fun inject(application: RecipesApplication)

    fun inject(homeFragment: RecipesFragment)
    fun inject(homeFragment: RecipeDetailsFragment)
}
@Module
abstract class AppContextModule {
    @Binds
    abstract fun bindAppContext(application: Application): Context
}
