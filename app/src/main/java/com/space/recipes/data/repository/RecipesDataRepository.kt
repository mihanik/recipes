package com.space.data.repository

import com.space.recipes.api.NetApiClient
import com.space.recipes.common_data.Recipe
import com.space.recipes.common_data.RecipesResponse
import javax.inject.Inject

interface RecipesDataRepository {
    suspend fun getData(): RecipesResponse<List<Recipe>, Exception?>
}

class RecipesDataRepositoryImpl @Inject constructor(private val netApiClient: NetApiClient) :
    RecipesDataRepository {
    override suspend fun getData(): RecipesResponse<List<Recipe>, Exception?> {
        return try {
            RecipesResponse.Success(netApiClient.fetchRecipe())
        } catch (e: Exception) {
            RecipesResponse.Error(e)
        }
    }
}
