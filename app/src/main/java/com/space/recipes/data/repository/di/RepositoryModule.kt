package com.space.data.repository.di

import com.space.data.repository.RecipesDataRepository
import com.space.data.repository.RecipesDataRepositoryImpl
import dagger.Binds
import dagger.Module

@Module
abstract class RepositoryModule {
    @Binds
    abstract fun bindRecipesRepository(repository: RecipesDataRepositoryImpl):
        RecipesDataRepository
}
