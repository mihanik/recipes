package com.space.recipes.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.space.recipes.common_data.Recipe
import com.space.recipes.databinding.ItemRecipeBinding
import com.space.recipes.extantions.loadImage

class RecipesAdapter :
    ListAdapter<Recipe, RecipesAdapter.RecipeAdapterViewHolder>(RecipesDiffCallback) {

    private var recipeSelectedListener: RecipeSelectedListener? = null

    fun setListener(recipeSelectedListener: RecipeSelectedListener) {
        this.recipeSelectedListener = recipeSelectedListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeAdapterViewHolder =
        RecipeAdapterViewHolder(
            ItemRecipeBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )

    override fun onBindViewHolder(holder: RecipeAdapterViewHolder, position: Int) =
        holder.bind(getItem(position))

    inner class RecipeAdapterViewHolder(private val binding: ItemRecipeBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Recipe) {
            binding.title.text = item.title
            item.photo?.let { binding.photoImageView.loadImage(it) }

            binding.root.setOnClickListener {
                recipeSelectedListener?.onRecipeSelected(item)
            }
        }
    }

    fun interface RecipeSelectedListener {
        fun onRecipeSelected(recipe: Recipe)
    }
}

private object RecipesDiffCallback : DiffUtil.ItemCallback<Recipe>() {
    override fun areItemsTheSame(oldItem: Recipe, newItem: Recipe): Boolean =
        oldItem.title == newItem.title

    override fun areContentsTheSame(oldItem: Recipe, newItem: Recipe): Boolean =
        oldItem == newItem
}
