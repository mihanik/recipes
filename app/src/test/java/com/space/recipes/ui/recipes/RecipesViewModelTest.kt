package com.space.recipes.ui.recipes

import com.jraska.livedata.test
import com.space.recipes.common_data.Recipe
import com.space.recipes.common_data.RecipesResponse
import com.space.recipes.interaction.GetRecipesUseCase
import com.space.recipes.utils.InstantTaskExecutorListener
import com.space.recipes.utils.TestCoroutineListener
import io.kotest.core.spec.IsolationMode
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.types.shouldBeInstanceOf
import io.mockk.coEvery
import io.mockk.mockk

class RecipesViewModelTest : BehaviorSpec({
    listeners(
        InstantTaskExecutorListener(),
        TestCoroutineListener(),
    )

    isolationMode = IsolationMode.InstancePerTest

    val getRecipesUseCase = mockk<GetRecipesUseCase>(relaxed = true)
    val viewModel = RecipesViewModel(getRecipesUseCase)

    Given("recipes viewModel") {
        val observer = viewModel.recipesState.test()

        When("viewModel initiated") {
            viewModel.loadRecipes()

            Then("state should be Init") {
                observer.value().shouldBeInstanceOf<ViewState.InitialState>()
            }
        }

        When("data loaded success") {
            val recipe = listOf(Recipe.empty.copy(title = "Cool recipe", chef = "Bimba"))
            val expected = RecipesResponse.Success<List<Recipe>, Exception?>(recipe)
            coEvery { getRecipesUseCase() } returns expected

            viewModel.loadRecipes()

            Then("state should be Success") {
                observer.value().shouldBeInstanceOf<ViewState.SuccessState>()
            }
        }

        When("loadRecipes called") {
            val expected = RecipesResponse.Error<List<Recipe>, Exception?>(Exception())
            coEvery { getRecipesUseCase() } returns expected

            viewModel.loadRecipes()

            Then("state should be Success") {
                Then("state should be Error") {
                    observer.value().shouldBeInstanceOf<ViewState.ErrorState>()
                }
            }
        }
    }
})
