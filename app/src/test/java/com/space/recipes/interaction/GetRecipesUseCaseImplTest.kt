package com.space.recipes.interaction

import com.space.data.repository.RecipesDataRepositoryImpl
import com.space.recipes.common_data.Recipe
import com.space.recipes.common_data.RecipesResponse
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.shouldBe
import io.mockk.coEvery
import io.mockk.mockk

class GetRecipesUseCaseImplTest : BehaviorSpec({
    val recipesDataRepository = mockk<RecipesDataRepositoryImpl>()
    val getRecipesUseCase = GetRecipesUseCaseImpl(recipesDataRepository)

    Given("recipe useCase") {
        When("response success") {
            val recipe = listOf(Recipe.empty.copy(title = "Cool recipe", chef = "Bimba"))
            val expected = RecipesResponse.Success<List<Recipe>, Exception?>(recipe)

            coEvery { recipesDataRepository.getData() } returns expected
            val result = getRecipesUseCase()

            Then("result should list of recipes") {
                result.shouldBe(expected)
            }
        }

        When("response error") {
            val exception = java.lang.Exception("Error")
            val expected = RecipesResponse.Error<List<Recipe>, Exception?>(exception)

            coEvery { recipesDataRepository.getData() } returns expected
            val result = getRecipesUseCase()

            Then("result should exception") {
                result.shouldBe(expected)
            }
        }
    }
})
