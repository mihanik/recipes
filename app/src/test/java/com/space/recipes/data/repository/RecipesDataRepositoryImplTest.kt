package com.space.recipes.data.repository

import com.space.data.repository.RecipesDataRepositoryImpl
import com.space.recipes.api.NetApiClient
import com.space.recipes.common_data.Recipe
import com.space.recipes.common_data.RecipesResponse
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.shouldBe
import io.mockk.coEvery
import io.mockk.mockk

class RecipesDataRepositoryImplTest : BehaviorSpec({

    val netApiClient = mockk<NetApiClient>()
    val recipesDataRepository = RecipesDataRepositoryImpl(netApiClient)

    Given("data repository") {
        When("response success") {
            val recipe = listOf(Recipe.empty.copy(title = "Cool recipe", chef = "Bimba"))
            coEvery { netApiClient.fetchRecipe() } returns recipe

            val expected = RecipesResponse.Success<List<Recipe>, Exception?>(recipe)
            val result = recipesDataRepository.getData()

            Then("result should list of recipes") {
                result.shouldBe(expected)
            }
        }

        When("response error") {
            val exception = Exception("Error")
            coEvery { netApiClient.fetchRecipe() } throws exception

            val expected = RecipesResponse.Error<List<Recipe>, Exception?>(exception)
            val result = recipesDataRepository.getData()

            Then("result should list of recipes") {
                result.shouldBe(expected)
            }
        }
    }
})
