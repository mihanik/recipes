# README #

This project is a test Marley Spoon technical challenge.

### What is this repository for? ###

* Application load and show recipes
* For running this project you need installed [Android Studio](https://developer.android.com/studio)
* Device simulator or connected android device

### How do I get set up? ###

* Clone project with git (git clone https://mihanik@bitbucket.org/mihanik/recipes.git)
* With android studio import project   
* Project includes all dependencies and does not require anything extra
* Build and run project with android studio
* For correct running unit test install kotest extension for Android Studio

### Who do I talk to? ###

* Project written with Kotlin
* Part of the project covered with unit tests